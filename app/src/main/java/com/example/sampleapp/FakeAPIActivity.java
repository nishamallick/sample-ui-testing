package com.example.sampleapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;
    /*
      A simple class that Opens up a dialog Box
      after 5 seconds of clicking the button
    */
public class FakeAPIActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fake_api);


        findViewById(R.id.fake_api_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EspressoIdlingResource.increment();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        openDialog();
                        if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
                            EspressoIdlingResource.decrement(); // Set app as idle.
                        }
                        Toast.makeText(FakeAPIActivity.this, "Toast after 5 Seconds",
                                Toast.LENGTH_LONG).show();


                    }
                }, 5000);
            }

        });


    }

    public void openDialog(){
        ExampleDialog exampleDialog = new ExampleDialog();
        exampleDialog.show(getSupportFragmentManager(), "example Dialog");

    }
}