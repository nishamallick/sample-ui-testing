package com.example.sampleapp;

public interface RecyclerViewClickInterface {
    void onItemClick(int position);
    void onLongItemClick(int position);
}
