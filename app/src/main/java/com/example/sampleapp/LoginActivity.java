package com.example.sampleapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    EditText uname, pass;
    Button login;
    Button pagerButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        uname=(EditText)findViewById(R.id.username);
        pass=(EditText)findViewById(R.id.password);
        login=(Button)findViewById(R.id.login_button);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uname.getText().toString().equals("admin") && pass.getText().toString().equals("admin123")) {
                    Toast.makeText(getApplicationContext(), "Redirecting...",Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(LoginActivity.this, Dashboard.class);
                    startActivity(intent);

                }else {

                    Toast.makeText(getApplicationContext(), "Wrong Credentials", Toast.LENGTH_SHORT).show();

                }
            }
        });
        /**
        Pager Button
         */
        pagerButton = findViewById(R.id.pager_button);

        pagerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, PagerActivity.class);
                startActivity(intent);
            }
        });

        /*
        Button for testing Fake API Calls
         */
        findViewById(R.id.login_api_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, FakeAPIActivity.class);
                startActivity(intent);
            }
        });

        /*
        Button for testing Sample Web View
         */
        findViewById(R.id.web_view_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, WebViewActivity.class);
                startActivity(intent);
            }
        });

    }
}
