package com.example.sampleapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.sampleapp.storagelocation.PermissionActivity;

public class Dashboard extends AppCompatActivity implements RecyclerViewClickInterface {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        RecyclerView listOfItems = (RecyclerView) findViewById(R.id.recycler_view);



        listOfItems.setLayoutManager(new LinearLayoutManager(this));
        String[] names = {"Thor", "Iron Man", "Captain America", "Hulk", "Vision", "Scarlett Witch",
                "Hawkeye", "Falcon", "Black Widow", "Black Panther",
                "Groot", "Star Lord", "Gamora", "Ant Man", "Spiderman", "Bucky Barnes", "Loki"};
        listOfItems.setAdapter(new ListAdapter(names, this));

    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(this, PermissionActivity.class);
        startActivity(intent);
        Toast.makeText(this, "Click Detected", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onLongItemClick(int position) {

        Toast.makeText(this, "Long Press Detected", Toast.LENGTH_SHORT).show();

    }
}