package com.example.sampleapp.storagelocation


import android.Manifest
import android.os.Build
import android.os.SystemClock.sleep
import android.util.Log
import android.view.View
import androidx.test.InstrumentationRegistry.getTargetContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.internal.platform.content.PermissionGranter
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiObjectNotFoundException
import androidx.test.uiautomator.UiSelector
import com.example.sampleapp.R
import junit.framework.TestCase.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class PermissionActivityTest {


    var permissionActivity: PermissionActivity? = null
    private var device : UiDevice? = null

    @get:Rule
    var permissionActivityTestRule = ActivityTestRule(PermissionActivity::class.java)

    @Before
    fun setUp() {
        this.device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        permissionActivity = permissionActivityTestRule.activity
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getInstrumentation().getUiAutomation().executeShellCommand(
                    "pm grant " + getTargetContext().getPackageName()
                            + " android.permission.WRITE_EXTERNAL_STORAGE")
        }
    }

    @Test
    fun locationClickTest() {
        assertNotNull(permissionActivity!!.findViewById<View>(R.id.btn_location))
        onView(withId(R.id.btn_location)).perform(click())
    }

    @Test
    fun storageClickTest() {
        assertNotNull(permissionActivity!!.findViewById<View>(R.id.btn_storage))
        onView(withId(R.id.btn_storage)).perform(click())
    }

    @Test
    fun grantPermissionTest(){
        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                PermissionGranter.GRANT_BUTTON_INDEX = 0
//            } else {
//                PermissionGranter.GRANT_BUTTON_INDEX = 1
//            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Thread.sleep(1000)
                val device =
                        UiDevice.getInstance(getInstrumentation())
                val allowPermissions = device.findObject(UiSelector()
                        .clickable(true)
                        .checkable(false)
                        .index(0))
                if (allowPermissions.exists()) {
                    allowPermissions.click()
                }
            }
        } catch (e: UiObjectNotFoundException) {
            Log.d("Test", "UiObjectNotFoundException $e")
        }
    }


    @Test
    fun allowPermissionsStorageTest() {
        try {
            val permissionNeeded: String = Manifest.permission.WRITE_EXTERNAL_STORAGE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                sleep(2000)
                val device = UiDevice.getInstance(getInstrumentation())
                val allowPermissions = device.findObject(UiSelector()
                        .clickable(true)
                        .checkable(false)
                        .index(0))
                if (allowPermissions.exists()) {
                    allowPermissions.click()
                }
            }
        } catch (e: UiObjectNotFoundException) {
            println("There is no permissions dialog to interact with")
        }
    }
}