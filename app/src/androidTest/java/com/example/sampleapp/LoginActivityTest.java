package com.example.sampleapp;

import android.app.Activity;
import android.app.Instrumentation;
import android.view.View;

import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertNotNull;


public class LoginActivityTest {

    @Rule
    public ActivityTestRule<LoginActivity> loginActivityActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    public LoginActivity  loginActivity = null;

    Instrumentation.ActivityMonitor monitor = getInstrumentation().
            addMonitor(Dashboard.class.getName(), null, false);

    @Before
    public void setUp() throws Exception {

        loginActivity = loginActivityActivityTestRule.getActivity();

    }

    @Test
    public void testEmptyField(){
        assertNotNull(loginActivity.findViewById(R.id.username));
        assertNotNull(loginActivity.findViewById(R.id.password));
    }

    @Test
    public void testTextField(){

        onView(withId(R.id.username)).perform(typeText("admin"));
        onView(withId(R.id.password)).perform(typeText("admin123"));
    }

    /**
     * Testing the launching of Second Activity
     * by clicking the Login Button
     */
    @Test
    public void testLaunchDashboardActivityOnClick(){
        assertNotNull(loginActivity.findViewById(R.id.login_button));
        onView(withId(R.id.username)).perform(typeText("admin"));
        onView(withId(R.id.password)).perform(typeText("admin123"));
        onView(withId(R.id.login_button)).perform(click());
        Activity secondActivity = getInstrumentation().waitForMonitorWithTimeout(monitor, 4000);
        assertNotNull(secondActivity);
        secondActivity.finish();
    }



    /**
     * Testing the launch of Activity
     */
    @Test
    public void launchActivityTest(){
        View view = loginActivity.findViewById(R.id.login_button);
        assertNotNull(view);
    }

    @After
    public void tearDown() throws Exception {
        loginActivity= null;
    }
}