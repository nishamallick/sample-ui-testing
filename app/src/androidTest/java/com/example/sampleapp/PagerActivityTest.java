package com.example.sampleapp;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Toast;

import androidx.test.espresso.NoActivityResumedException;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

public class PagerActivityTest {
    @Rule
    public ActivityTestRule<PagerActivity> pagerActivityActivityTestRule =
            new ActivityTestRule<>(PagerActivity.class);

    public PagerActivity pagerActivity = null;

    @Before
    public void setUp() throws Exception {
        pagerActivity = pagerActivityActivityTestRule.getActivity();
    }

    /**
     * Testing the launch of Fragment is Loaded or Not
     */
    @Test
    public void launchActivityTest(){
        View view = pagerActivity.findViewById(R.id.txt_display);
        assertNotNull(view);
    }

    /**
     To test the Swipes on the Fragments
     */
    @Test
    public void fragmentSwipeTest(){
        onView(withId(R.id.pager)).perform(ViewActions.swipeLeft(),
                swipeLeft(), swipeLeft(),
                swipeRight(),
                swipeRight(),
                swipeLeft(),
                swipeUp(),
                swipeDown());
    }

    /**
     * Back Press Test
     */
    @Test
    public void backPressDetected() {
        try {
            pressBack();
            fail("Should have thrown NoActivityResumedException");
        } catch (NoActivityResumedException expected) {
        }
    }

    @After
    public void tearDown() throws Exception {

        /*
        Trying to Toast a "Successfull Message" in tearDown()
         */
        
//        Toast.makeText(pagerActivity.getApplicationContext(), "Tested Successfully", Toast.LENGTH_LONG).show();
//        Thread thread = new Thread(){
//            public void run(){
//                Looper.prepare();//Call looper.prepare()
//
//                Handler mHandler = new Handler() {
//                    public void handleMessage(Message msg) {
//                        Toast.makeText(pagerActivity.getApplicationContext(), "Tested Successfully", Toast.LENGTH_LONG);
//                    }
//                };
//
//                Looper.loop();
//            }
//        };
//        thread.start();
        
        pagerActivity = null;
    }
}