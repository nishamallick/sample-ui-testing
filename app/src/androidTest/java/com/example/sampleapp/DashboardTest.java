package com.example.sampleapp;

import android.app.Activity;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.NoActivityResumedException;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.Espresso.pressBackUnconditionally;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class DashboardTest {

    @Rule
    public ActivityTestRule<Dashboard> dashboardActivityTestRule =
            new ActivityTestRule<>(Dashboard.class);

    /**
     * To test that is View Visible
     */
    @Test
    public void isRecyclerVisible(){

//        Dashboard activity = dashboardActivityTestRule.getActivity();
//
        Espresso.onView(withId(R.id.recycler_view)).check(matches(isDisplayed()));
//        onView(withText(R.string.app_name)).
//                inRoot(withDecorView(not(is(activity.getWindow().getDecorView())))).
//                check(matches(isDisplayed()));
////        Toast.makeText(dashboardActivityTestRule.getActivity(), "Views Are Visible",
////                Toast.LENGTH_SHORT).show();
    }

    /**
    To test the Clicks on the item of Recycler View
     */
    @Test
    public void recyclerClickTest(){
        onView(withId(R.id.recycler_view)).
                perform(RecyclerViewActions.actionOnItemAtPosition(1,click()));
    }

    /**
     * Back Press Test
     */
    @Test
    public void backPressDetected(){
        try {
            pressBack();
            fail("Should have thrown NoActivityResumedException");
        }
        catch (NoActivityResumedException expected) {
        }
//        assertTrue(dashboardActivityTestRule.activity.isDestroyed);
//        onView(withId(R.id.login_button)).check(matches(isDisplayed()));
    }



}