package com.example.sampleapp;

import android.view.View;

import androidx.test.espresso.IdlingRegistry;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

public class FakeAPIActivityTest {
    @Rule
    public ActivityTestRule<FakeAPIActivity> fakeAPIActivityTestRule =
            new ActivityTestRule<>(FakeAPIActivity.class);

    public FakeAPIActivity  fakeAPIActivity = null;

    @Before
    public void setUp() throws Exception {
        fakeAPIActivity = fakeAPIActivityTestRule.getActivity();


        // Registering Idling Resource before any tests regarding this component
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getIdlingResource());
    }


    /**
     * Testing the launch of Activity
     */
    @Test
    public void launchActivityTest(){
        View view = fakeAPIActivity.findViewById(R.id.fake_api_button);
        assertNotNull(view);
    }


    @Test
    public void testCheckDialogDisplayed() {
        // Click on the button that shows the dialog
        onView(withId(R.id.fake_api_button)).perform(click());

        // Check the dialog title text is displayed
        onView(withText("Sample Dialog Box")).check(matches(isDisplayed()));
    }




    @After
    public void tearDown() throws Exception {

        // Unregistering Idling Resource so it can be garbage collected and does not leak any memory
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getIdlingResource());
    }
}